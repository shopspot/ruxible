import React from 'react';
import TestUtils from 'react-addons-test-utils'
import { render, unmountComponentAtNode, findDOMNode } from 'react-dom'
import { jsdom } from 'jsdom'
import sinon from 'sinon'

import RuxibleComponent from '../src/RuxibleComponent'
import RuxibleContext from '../src/RuxibleContext'
import StoreContainer from '../src/StoreContainer'
import AppDispatcher from '../src/AppDispatcher'
import { getStoreType, executeActionType } from '../src/ContextType'

describe('RuxibleComponent', () => {
    let node;

    class ContextComponent extends React.Component{
        static defaultProps = {
            rxContext: new RuxibleContext(new StoreContainer(), new AppDispatcher())
        };

        static childContextTypes = {
            rxContext: React.PropTypes.instanceOf(RuxibleContext)
        };

        getChildContext() {
            return {
                rxContext: this.props.rxContext,
            };
        }

        render() { return (this.props.children); }
    };

    before((done) => {
        jsdom.env(
            '<html><header></header><body></body></html>',
            function(err, window) {
                global.document = window.document;
                global.window = window;
                done();
            }
        );
    });

    after(() => {
        delete global.document;
        delete global.window;
    });

    beforeEach(() => {
        node = document.createElement('div');
    });

    afterEach(() => {
        unmountComponentAtNode(node);
    });

    it('should pass context to children', (done) => {
        class ChildComponent extends React.Component {
            static contextTypes = {
                getStore: getStoreType,
                executeAction: executeActionType,
            };

            constructor(props, context) {
                super(props, context);
                context.getStore.should.be.ok();
                context.executeAction.should.be.ok();
                done();
            }

            render() { return <div></div> }

        };

        ChildComponent.contextTypes = {
            getStore: getStoreType,
            executeAction: executeActionType,
        };

        render(
            <ContextComponent>
                <RuxibleComponent>
                    <ChildComponent/>
                </RuxibleComponent>
            </ContextComponent>
        , node);
    });

});
