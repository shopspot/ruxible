import sinon from 'sinon'
import should from 'should'
import Path from '../src/Path'
import RuxibleRouter from '../src/RuxibleRouter'
import RuxibleComponent from '../src/RuxibleComponent'
import StoreContainer from '../src/StoreContainer'
import AppDispatcher from '../src/AppDispatcher'
import RuxibleContext from '../src/RuxibleContext'

describe('Router', () => {

    describe('#parseRouteConfig', () => {
        let Main = () => {}, Index = () => {}, About = () => {}, User = () => {}, Friend = () => {};
        let createElement = () => {};

        it('shoud be parse properly with properly config', () => {
            let pathCfg = {
                path: '/',
                component: Main,
                childs: [
                    { index: true, component: Index },
                    { path: 'about', component: About },
                    {
                        path: 'user',
                        component: User,
                        childs: [ { path: '/friend', component: Friend } ]
                    }
                ]
            };

            let expected = {
                path: '/',
                component: Main,
                indexRoute: { component: Index },
                childRoutes: [
                    { path: 'about', component: About},
                    {
                        path: 'user',
                        component: User,
                        childRoutes: [ { path: '/friend', component: Friend } ]
                    }
                ]
            };

            let ctx = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let router = new RuxibleRouter(Path.createPath(pathCfg));
            let routeCfg = router.createRouterCfg();

            routeCfg.should.containDeep(expected);
        });

        it('shoud invoke onEnter and onLeave when set', () => {
            let pathCfg = {
                path: '/',
                component: Main,
                childs: [
                    { index: true, component: Index },
                    { path: 'about', component: About },
                ]
            };

            let enterExpectedCall = sinon.spy();
            let leaveExpectedCall = sinon.spy();

            let ctx = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let path = Path.createPath(pathCfg);
            let router = new RuxibleRouter(path);

            router.setOnEnterRoute(enterExpectedCall);
            router.setOnLeaveRoute(leaveExpectedCall);

            let routeCfg = router.createRouterCfg();
            routeCfg.onEnter({}, {}, () => {});
            routeCfg.onLeave({}, {});

            enterExpectedCall.calledWith(path).should.be.true();
            leaveExpectedCall.calledWith(path).should.be.true();
        });

        it('shoud invoke config onEnter and onLeave when set', () => {
            let expectedOnEnter = sinon.spy((nextState, replaceState, cb) => { cb(false); });
            let expectedOnLeave = sinon.spy((nextState, replaceState) => { return false; });
            let expectedOnEnterRoute = sinon.spy();
            let expectedOnLeaveRoute = sinon.spy();

            let pathCfg = {
                path: '/',
                component: Main,
                onEnter: expectedOnEnter,
                onLeave: expectedOnLeave,
                childs: [
                    { index: true, component: Index },
                    { path: 'about', component: About },
                ]
            };

            let ctx = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let path = Path.createPath(pathCfg);
            let router = new RuxibleRouter(path);

            router.setOnEnterRoute(expectedOnEnterRoute);
            router.setOnLeaveRoute(expectedOnLeaveRoute);

            let routeCfg = router.createRouterCfg();
            routeCfg.onEnter();
            routeCfg.onLeave();

            expectedOnEnter.called.should.be.true();
            expectedOnLeave.called.should.be.true();
            expectedOnEnterRoute.calledWith(path).should.be.true();
            expectedOnLeaveRoute.calledWith(path).should.be.true();
        });

    });

    it('shoud invoke config onEnter and onLeave when set', () => {
        let Main = () => {}, Index = () => {}, About = () => {};

        let expectedOnEnter = sinon.spy((nextState, replaceState, cb) => { cb(true); });
        let expectedOnLeave = sinon.spy((nextState, replaceState) => { return true; });
        let expectedOnEnterRoute = sinon.spy();
        let expectedOnLeaveRoute = sinon.spy();

        let pathCfg = {
            path: '/',
            component: Main,
            onEnter: expectedOnEnter,
            onLeave: expectedOnLeave,
            childs: [
                { index: true, component: Index },
                { path: 'about', component: About },
            ]
        };

        let ctx = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let path = Path.createPath(pathCfg);
        let router = new RuxibleRouter(path);

        router.setOnEnterRoute(expectedOnEnterRoute);
        router.setOnLeaveRoute(expectedOnLeaveRoute);

        let routeCfg = router.createRouterCfg();
        routeCfg.onEnter();
        routeCfg.onLeave();

        expectedOnEnter.called.should.be.true();
        expectedOnLeave.called.should.be.true();
        expectedOnEnterRoute.calledWith(path).should.be.false();
        expectedOnLeaveRoute.calledWith(path).should.be.false();
    });

});
