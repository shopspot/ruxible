import sinon from 'sinon'
import should from 'should'
import React from 'react'

import RuxibleContext from '../src/RuxibleContext'
import StoreContainer from '../src/StoreContainer'
import AppDispatcher from '../src/AppDispatcher'
import routing from '../src/routing'
import Path from '../src/Path'

describe('routing', () => {
    let mockPromise = (result) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(result); }, 100);
        });
    }

    it('action should be call with payload', (done) => {
        let rxContext = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let action1 = sinon.spy(() => { return mockPromise({}); }),
            action2 = sinon.spy(() => { return mockPromise({}); });
        let expectContext = rxContext.getActionContext();
        let expectPayload = { name: 'payload' };

        class MockComponent extends React.Component {
            render() { return (<span>hello</span>); }
        };

        let path = Path.createPath({
            path: '/',
            component: MockComponent,
            actions: [{
                action: action1,
                payload: expectPayload
            }, {
                action: action2
            }]
        });

        routing(path, '/')
            .then((redirectLocation, renderProps) => {
                action1.calledWith(sinon.match.any, expectPayload, sinon.match.func).should.be.true();
                action2.calledWith(sinon.match.any, sinon.match({}), sinon.match.func).should.be.true();
                done();
            })
            .catch(done);
    });

    it('action should be call with payload', (done) => {
        let action1 = sinon.spy(() => { return mockPromise({}); }),
            action2 = sinon.spy(() => { return mockPromise({}) });
        let expectPayload = { name: 'payload' };
        let expectOverridePayload = { name: 'override' };

        class MockComponent extends React.Component {
            render() { return <span>hello</span> }
        };

        routing(Path.createPath({
            path: '/',
            component: MockComponent,
            actions: [{
                action: action1,
                payload: expectPayload
            }, {
                action: action2,
                payload: expectOverridePayload
            }]
        }), '/')
            .then((redirectLocation, renderProps) => {
                action1.calledWith(sinon.match.any, expectPayload).should.be.true();
                action2.calledWith(sinon.match.any, expectOverridePayload).should.be.true();
                done();
            })
            .catch(done)
    });

});
