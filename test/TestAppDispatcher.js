import should from 'should'
import AppDispatcher from '../src/AppDispatcher'

describe('AppDispatcher', () => {
    describe('#register-dispatch', () => {
        it('register with default channel callback should be called', () => {
            let expected = {
                name: 'default'
            };
            let output = {};
            let callAmount = 0;

            let dp = new AppDispatcher();
            dp.register((payload) => {
                output = payload;
                callAmount++;
            });

            dp.dispatch(expected);

            output.should.be.eql(expected);
        });

        it('register with specific channel callback should be called', () => {
            let expected = {
                name: 'default'
            };
            let output = {};

            let dp = new AppDispatcher();
            dp.register('DEV', (payload) => {
                output = payload;
            });

            dp.dispatch('DEV', expected);

            output.should.be.eql(expected);
        });

        it('callback should be call once', () => {
            let expected = 1;
            let output = 0;

            let dp = new AppDispatcher();
            dp.register('DEV1', (payload) => {});
            dp.register('DEV2', (payload) => {
                output++;
            });


            dp.dispatch('DEV2', {});

            output.should.be.eql(expected);
        });

        it('register with default channel specific channdle callback should not be called', () => {
            let expected = {
                name: 'default'
            };
            let output = {};

            let dp = new AppDispatcher();
            dp.register('DEV', (payload) => {
                output = payload;
            });

            dp.dispatch(expected);

            output.should.not.be.eql(expected);
        });

        it('dependency should be called and have same payload', () => {
            let expected = 3;
            let output = 0;
            let callOrder = 0;
            let exPayload = {
                message: 'hello'
            };

            let dp = new AppDispatcher();
            dp.register('child1', (payload) => {
                output++;
                callOrder++;
                payload.should.be.eql(exPayload);
            });
            dp.register('child2', (payload) => {
                output++;
                callOrder++;
                payload.should.be.eql(exPayload);
            });
            dp.register('child3', (payload) => {
                output++;
                callOrder++;
                payload.should.be.eql(exPayload);
            });
            dp.register('parent', (payload) => {
                callOrder.should.be.eql(expected);
            }, ['child1', 'child2', 'child3']);

            dp.dispatch('parent', exPayload);

            output.should.be.eql(expected);
        });

    });
});
