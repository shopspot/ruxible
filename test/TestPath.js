import should from 'should';
import Path from '../src/Path';

describe('Path', () => {

    describe('#createPath', () => {
            let Main = () => {}, Index = () => {}, About = () => {}, User = () => {};
            let UserStore = () => {}, SessionStore = () => {};

            let pathCfg = {
                path: '/',
                component: Main,
                childs: [
                    { index: true, component: Index },
                    { path: 'about', component: About },
                    {
                        path: 'user',
                        component: User,
                        stores: [UserStore, SessionStore]
                    }
                ]
            };

            let path = Path.createPath(pathCfg);
            path.should.be.ok;

            let deepCompare = (path, config) => {
                path.getPath().should.be.equal(config.path || '');
                path.getComponent().should.be.equal(config.component);
                path.getIndex().should.be.equal(config.index || false);

                for (let index in path.getStores()) {
                    let expect = config.stores[index];
                    let store = path.getStores()[index];
                    store.should.be.equal(expect);
                }

                for (let index in path.getChilds()) {
                    let expect = config.childs[index];
                    let child = path.getChilds()[index];
                    deepCompare(child, expect);
                }
            };

            deepCompare(path, pathCfg);
    });

});
