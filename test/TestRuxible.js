import sinon from 'sinon'
import should from 'should'
import jsdom from 'jsdom'
import createHistory from 'history/lib/createMemoryHistory'
import React from 'react'
import ReactTestUtils from 'react-addons-test-utils'
import { render, unmountComponentAtNode } from 'react-dom'

import Ruxible from '../src/Ruxible'
import RuxibleComponent from '../src/RuxibleComponent'
import RuxibleContext from '../src/RuxibleContext'
import StoreContainer from '../src/StoreContainer'
import AppDispatcher from '../src/AppDispatcher'
import Path from '../src/Path'
import { getStoreType, executeActionType, urlType } from '../src/ContextType'

import execSteps from './execSteps'

class MainComponent extends React.Component {

    render() {
        return (
            <div>{ this.props.children }</div>
        );
    }

}

describe('Ruxible', () => {
    let node;
    let store;
    let ctx;

    let store1 = class MockStore{};
    let store2 = class MockStore{};

    store1.storeName = 'store1';
    store2.storeName = 'store2';

    before((done) => {
        jsdom.env('<html><body></body></html>', [], (err, window) => {
            global.window = window;
            global.document = window.document;
            global.navigator = window.navigator;

            done();
        });
    });

    after(() => {
        delete global.document;
        delete global.window;
    });

    beforeEach(() => {
        node = document.createElement('div');

        store = new StoreContainer();
        ctx = new RuxibleContext(store, new AppDispatcher());
    });

    afterEach(() => {
        unmountComponentAtNode(node);
    });


    it('config store should be register', (done) => {
        store.registerStore = sinon.spy();

        class MockComponent extends React.Component {
            render() { return <span>Hello</span> }
        }

        let path = Path.createPath({
            path: '/',
            component: MockComponent,
            stores: [store1, store2]
        });

        render(<Ruxible path={path} history={createHistory('/')} rxContext={ctx} />, node, () => {
            store.registerStore.calledWith(store1).should.be.true();
            store.registerStore.calledWith(store2).should.be.true();
            done();
        });

    });

    it('config store should be register all store on routing path', (done) => {
        store.registerStore = sinon.spy();

        class MockComponent extends React.Component {
            render() { return <span>Hello</span> }
        }

        class SubComponent extends React.Component {

            render() { return <span>Hello</span> }

        }

        let path = Path.createPath({
            path: '/',
            component: MainComponent,
            stores: [store1],
            childs: [{
                path: '/path1/:id',
                component: SubComponent,
                stores: [store2]
            }]
        });

        render(<Ruxible path={path} history={createHistory('/path1/aaaaa')} rxContext={ctx} />, node, () => {
            store.registerStore.calledWith(store1).should.be.true();
            store.registerStore.calledWith(store2).should.be.true();
            done();
        });

    });

    it('store should not be unregister when new path was the same', (done) => {
        class MainComponent extends React.Component {

            render() { return <span>Hello</span> }

        }

        class SubComponent extends React.Component {

            render() { return <span>Hello</span> }

        }

        let history = createHistory('/path1/aaaaa');
        let path = Path.createPath({
            path: '/',
            component: MainComponent,
            stores: [store1],
            childs: [{
                path: '/path1/:id',
                component: SubComponent,
                stores: [store2]
            }]
        });

        store.unregisterStore = sinon.spy();

        render(<Ruxible path={path} history={history} rxContext={ctx} />, node, () => {
            history.pushState(null, '/path1/bbbbb');

            store.unregisterStore.calledWith(store1).should.be.false();
            store.unregisterStore.calledWith(store2).should.be.false();
            done();
        });
    })

    it('ruxible should be pass component context to RuxibleComponent', (done) => {
        let expect = ctx.getComponentContext();

        class ContextComponent extends React.Component {

            render() {
                let output = this.context;
                output.should.containEql(ctx.getComponentContext());
                done();

                return <div></div>;
            }
        }
        ContextComponent.contextTypes = {
            getStore: getStoreType,
            executeAction: executeActionType
        };

        let path = Path.createPath({
            path: '/',
            component: ContextComponent
        });

        render(<Ruxible path={path} history={createHistory('/')} rxContext={ctx} />, node);
    });

});
