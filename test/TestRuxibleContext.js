import should from 'should'
import RuxibleContext from '../src/RuxibleContext.js'
import StoreContainer from '../src/StoreContainer.js'
import AppDispatcher from '../src/AppDispatcher.js'
import MockStore from './mock/MockStore.js'

describe('RuxibleContext', () => {

    describe("#contructor", () => {

    });

    describe('#getActionContext', () => {

        it('should have executeAction function', () => {
            let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let actionContext = context.getActionContext();

            actionContext.should.be.ok();
            actionContext.executeAction.should.be.ok();
            actionContext.executeAction.should.be.Function();
        });

        it('should have getStore function', () => {
            let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let actionContext = context.getActionContext();

            actionContext.getStore.should.be.ok();
            actionContext.getStore.should.be.Function();
        });

        it('should have dispatch function', () => {
            let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let actionContext = context.getActionContext();

            actionContext.dispatch.should.be.ok();
            actionContext.dispatch.should.be.Function();
        });

        describe('#executeAction', () => {
            it('should return promise with action return promise', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-promise > return promise';

                let mockAction = (context, payload) => {
                    return new Promise((resolve, reject) => {
                        resolve(payload);
                    });
                }

                let actionContext = context.getActionContext();
                let resultPromise = actionContext.executeAction(mockAction, expectPayload);
                resultPromise.should.be.ok();
                resultPromise
                    .then(data => {
                        data.should.be.ok();
                        data.should.be.eql(expectPayload);
                        done();
                    })
                    .catch(function(error) {
                        done(error);
                    });
            });

            it('should call callback with action return promise', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-promise > call callback';

                let mockAction = (context, payload) => {
                    return new Promise((resolve, reject) => {
                        resolve(payload);
                    });
                }

                let actionContext = context.getActionContext();
                actionContext.executeAction(mockAction, expectPayload, (err, result) => {
                    try {
                        result.should.be.ok();
                        result.should.be.eql(expectPayload);
                        done();
                    } catch (error) {
                        done(error);
                    }
                });
            });

            it('should return promise with action callback', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-callback > return promise';

                let mockCallbackAction = (context, payload, callback) => {
                    callback(null, payload);
                };

                let actionContext = context.getActionContext();
                let resultPromise = actionContext.executeAction(mockCallbackAction, expectPayload);

                resultPromise.should.be.ok();
                resultPromise
                    .then(data => {
                        data.should.be.ok();
                        data.should.be.eql(expectPayload);
                        done();
                    })
                    .catch(function(error) {
                        done(error);
                    });
            });

            it('should call callback with action callback', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-promise > call callback';

                let mockCallbackAction = (context, payload, callback) => {
                    callback(null, payload);
                }

                let actionContext = context.getActionContext();
                actionContext.executeAction(mockCallbackAction, expectPayload, (err, result) => {
                    try {
                        result.should.be.ok();
                        result.should.be.eql(expectPayload);
                        done();
                    } catch (error) {
                        done(error);
                    }
                });
            });
        });

    });

    describe('#getComponentContext', () => {

        it('should have executeAction function', () => {
            let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let actionContext = context.getComponentContext();

            actionContext.should.be.ok();
            actionContext.executeAction.should.be.ok();
            actionContext.executeAction.should.be.Function();
        });

        it('should have getStore function', () => {
            let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
            let actionContext = context.getComponentContext();

            actionContext.getStore.should.be.ok();
            actionContext.getStore.should.be.Function();
        });

        describe('#executeAction', () => {
            it('should not return promise with action return promise', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-promise > return promise';

                let mockAction = (context, payload) => {
                    return new Promise((resolve, reject) => {
                        resolve();
                        done();
                    });
                }

                let actionContext = context.getComponentContext();
                actionContext.executeAction(mockAction, expectPayload);
            });

            it('should not return promise with action callback', (done) => {
                let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                let expectPayload = 'action-callback > return promise';

                let mockCallbackAction = (context, payload, callback) => {
                    callback(null, payload);
                    done();
                }

                let actionContext = context.getComponentContext();
                actionContext.executeAction(mockCallbackAction, expectPayload);
            });
        });

    });

    describe('#registerStore', () => {

        it('with store should be success', () => {
            let dispatcher = new AppDispatcher();
            let storeContainer = new StoreContainer();

            let ctx = new RuxibleContext(storeContainer, dispatcher);
            let msg = 'dispatch';

            let calledParent = false;
            let calledChild = false;

            MockStore.prototype.testEvent = (payload) => {
                payload.should.be.ok();
                payload.should.be.String();
                payload.should.be.eql(msg);

                calledParent = true;
            };

            MockStore.prototype.chainEvent = (payload) => {
                payload.should.be.ok();
                payload.should.be.String();
                payload.should.be.eql(msg);

                calledChild = true;
            };

            ctx.registerStore(MockStore);
            dispatcher.dispatch('TEST_EVENT', msg);

            calledParent.should.be.true();
            calledChild.should.be.true();
        });

        it('with non-store should be unsuccess', () => {
            (() => {
                let ctx = new RuxibleContext(new StoreContainer(), new AppDispatcher());
                ctx.registerStore({});
            }).should.throw();
        });

    });

});
