import sinon from 'sinon';
import should from 'should';
import chainAction from '../src/addon/ChainAction';
import RuxibleContext from '../src/RuxibleContext';
import StoreContainer from '../src/StoreContainer';
import AppDispatcher from '../src/AppDispatcher';

describe('ChainPromise', () => {

    it('should call promise with order', () => {
        let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let promise = new Promise((resolve, reject) => {
            setTimeout(resolve, 100);
        });

        let action1 = sinon.spy(() => { return promise; });
        let action2 = sinon.spy(() => { return promise; });
        let action3 = sinon.spy(() => { return promise; });

        let payload = {
            say: 'Hello World'
        };

        let pAction = chainAction(context.getActionContext())
            .next(action1, payload)
            .next(action2)
            .next(action3)
            .execute();


        return pAction.then(() => {
            action1.calledBefore(action2).should.be.true();
            action2.calledBefore(action3).should.be.true();
            action3.called.should.be.true();
        });
    });

    it('should forward param', () => {
        let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let promise = (payload) => {
            return new Promise((resolve, reject) => {
                resolve(payload);
            });
        };

        let expectPayload1 = {
                say: 'Hello World'
            },
            expectPayload2 = {
                say: 'Hello World!!'
            },
            expectPayload3 = {
                say: 'Hello World!!!!'
            };


        let action1 = sinon.spy((ctx, payload) => { return promise(expectPayload2); }),
            action2 = sinon.spy((ctx, payload) => { return promise(expectPayload3); }),
            action3 = sinon.spy((ctx, payload) => { return promise(); });


        let pAction = chainAction(context.getActionContext())
            .next(action1, expectPayload1)
            .next(action2)
            .next(action3)
            .execute();


        return pAction.then(() => {
            let actionCtx = context.getActionContext();

            action1.args[0][0].should.deepEqual(actionCtx);
            action2.args[0][0].should.deepEqual(actionCtx);
            action3.args[0][0].should.deepEqual(actionCtx);

            action1.args[0][1].should.deepEqual(expectPayload1);
            action2.args[0][1].should.deepEqual(expectPayload2);
            action3.args[0][1].should.deepEqual(expectPayload3);
        });
    });

    it('override param should be success', () => {
        let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let promise = (payload) => {
            return new Promise((resolve, reject) => {
                resolve(payload);
            });
        };

        let expectParam = { text: 'Hello' };

        let action1 = sinon.spy((ctx, payload) => { return promise({}); }),
            action2 = sinon.spy((ctx, payload) => { return promise({}); }),
            action3 = sinon.spy((ctx, payload) => { return promise(); });


        let pAction = chainAction(context.getActionContext())
            .next(action1, {})
            .next(action2, expectParam)
            .next(action3)
            .execute();


        return pAction.then(() => {
            let actionCtx = context.getActionContext();

            action1.args[0][1].should.deepEqual({});
            action2.args[0][1].should.deepEqual(expectParam);
            action3.args[0][1].should.deepEqual({});
        });
    });

    it('no action should return nothing', () => {
        let context = new RuxibleContext(new StoreContainer(), new AppDispatcher());
        let pAction = chainAction(context.getActionContext()).execute();

        return pAction.then(() => {

        }).catch((err) => {
            err.should.not.be.ok();
        });
    });
});
