import BaseStore from '../../src/BaseStore.js';

class MockStore extends BaseStore {

    constructor() {
        super();
        this._list = [];
    }

    getList() {
        return this._list;
    }

    addObj(obj) {
        this._list.push(obj);
    }

    serialize() {
        return this._list;
    }

    deserialize(des) {
        this._list = des;
    }

    testEvent(payload) {

    }

    chainEvent(payload) {

    }

}

MockStore.storeName = 'mockStore';
MockStore.events = {
    'TEST_EVENT': 'testEvent',
    'CHAIN_EVENT': 'chainEvent'
};
MockStore.dependencies = {
    'TEST_EVENT': ['CHAIN_EVENT']
}

export default MockStore;
