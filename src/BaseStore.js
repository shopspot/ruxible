import {EventEmitter} from 'events';
import {AppDispatcher} from './AppDispatcher.js';

const CHANGE_EVENT = 'change';

class BaseStore extends EventEmitter {

    constructor() {
        super();
        this.name = this.constructor.storeName;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

}

module.exports = BaseStore;
