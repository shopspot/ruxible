import {Dispatcher} from 'flux';

const DEFAULT_CHANNEL = 'default';

class ChannelDispatcher {

    constructor() {
        this._dispatcher = new Dispatcher();
        this._channels = {};
    }

    register(channel, cb, dependencies) {
        dependencies = dependencies || [];
        if (!cb) {
            cb = channel;
            channel = DEFAULT_CHANNEL;
        }

        this.validate(channel, dependencies);

        this._channels[channel] = {
            handler: cb,
            dependencies: dependencies
        };
    }

    unregister(channel) {
        delete this._channels[channel];
    }

    setDependencies(channel, dependencies) {
        if (!this.isExistsChannel(channel)) {
            throw new Error('Channel "' + channel + '" is not exists');
        }

        this._channels[channel].dependencies = dependencies;
    }

    validate(channel, dependencies) {
        if (this.isExistsChannel(channel)) {
            throw new Error('Channel already exists');
        }

        for (var index in dependencies) {
            if (!this.isExistsChannel(dependencies[index]))
                throw new Error('Dependencies "' + dependencies[index] + '" doesn\' exists in channel');
        }
    }

    dispatch(channelName, payload) {
        if (!payload) {
            payload = channelName;
            channelName = DEFAULT_CHANNEL;
        }

        if (!this.isExistsChannel(channelName)) {
            return;
        }

        let channel = this._channels[channelName];
        let depIds = [];
        let depends = channel.dependencies;


        for (let index in depends) {
            let depCh = this._channels[depends[index]];
            let id = this._dispatcher.register(depCh.handler);
            depIds.push(id);
        }

        let id = 0;
        if (depIds.length > 0) {
            id = this._dispatcher.register((payload) => {
                this.waitFor(depIds);
                channel.handler(payload);
            });
        }
        else {
            id = this._dispatcher.register(channel.handler);
        }


        this._dispatcher.dispatch(payload);

        this._dispatcher.unregister(id);
        for (let index in depIds) {
            this._dispatcher.unregister(depIds[index]);
        }
    }

    waitFor(ids) {
        this._dispatcher.waitFor(ids);
    }

    isMultipleArgs(args) {
        return args.length > 1;
    }

    isExistsChannel(channel) {
        return this._channels[channel] ? true : false;
    }

}

module.exports = ChannelDispatcher;
