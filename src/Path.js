class Path {

    constructor(path, component) {
        this.path = path || "";
        this.component = component;
        this.actions = [];
        this.stores = [];
        this.childs = [];
        this.index = false;
    }

    getPath() {
        return this.path;
    }

    getComponent() {
        return this.component;
    }

    setAction(act, payload) {
        this.actions.push({
            action: act,
            payload: payload
        });
    }

    getActions() {
        return this.actions;
    }

    addStore(store) {
        this.stores.push(store);
    }

    getStores() {
        return this.stores;
    }

    addChild(child) {
        this.childs.push(child);
    }

    getChilds() {
        return this.childs;
    }

    setIndex() {
        this.index = true;
    }

    getIndex() {
        return this.index;
    }

    setOnEnter(onEnter) {
        this.onEnter = onEnter;
    }

    getOnEnter() {
        return this.onEnter;
    }

    setOnLeave(onLeave) {
        this.onLeave = onLeave;
    }

    getOnLeave() {
        return this.onLeave;
    }

    static createPath(pathConfig) {
        let makePath = (pathCfg) => {
            let path = new Path(pathCfg.path, pathCfg.component);
            if (pathCfg.index && pathCfg.index === true) {
                path.setIndex();
            }

            let stores = pathCfg.stores || [];
            for (let index in stores) {
                let store = stores[index];
                path.addStore(store);
            }

            let childs = pathCfg.childs || [];
            for (let index in childs) {
                let child = childs[index];
                path.addChild(makePath(child));
            }

            let actions = pathCfg.actions || [];
            for (let index in actions) {
                let action = actions[index];
                path.setAction(action.action, action.payload);
            }

            if (pathCfg.onEnter) {
                path.setOnEnter(pathCfg.onEnter);
            }

            if (pathCfg.onLeave) {
                path.setOnLeave(pathCfg.onLeave);
            }

            return path;
        }

        return makePath(pathConfig);
    }

};

module.exports = Path;
