import React from 'react'
import { match, RoutingContext } from 'react-router'

import RuxibleContext from './RuxibleContext'
import RuxibleComponent from './RuxibleComponent'
import StoreContainer from './StoreContainer'
import AppDispatcher from './AppDispatcher'
import RuxibleRouter from './RuxibleRouter'
import Path from './Path'
import ChainAction from './addon/ChainAction'
import { getStoreType, executeActionType } from './ContextType.js'

let createElement = (Component, props) => {
    return (
        <RuxibleComponent rxStores={props.route.stores}>
            <Component {...props} />
        </RuxibleComponent>
    );
}

class ContextComponent extends React.Component {
    static childContextTypes = {
        rxContext: React.PropTypes.instanceOf(RuxibleContext)
    };

    constructor(props) {
        super(props);
    }

    getChildContext() {
        return {
            rxContext: this.props.rxContext
        };
    }

    render() {
        return React.cloneElement(this.props.children, {});
    }

};

module.exports = (path, url) => {
    let rxContext = new RuxibleContext(new StoreContainer(), new AppDispatcher());
    let router =  new RuxibleRouter(Path.createPath(path));

    return new Promise((resolve, reject) => {
        match({ routes: router.createRouterCfg(), location: url}, (error, redirectLocation, renderProps) => {
            if (error)
                reject(error);
            else {
                renderProps.createElement = createElement;

                let routes = renderProps.routes;
                let promises = [];

                routes.forEach( route => {
                    let actions = route.actions || [];
                    let stores = route.stores || [];

                    let chain = ChainAction(rxContext.getActionContext());

                    stores.forEach( store => {
                        rxContext.registerStore(store);
                    });

                    actions.forEach( action => {
                        let payload = action.payload || {};
                        payload.params = renderProps.params;
                        payload.location = renderProps.location;

                        chain.next(action.action, payload);
                    });

                    promises.push(chain.execute());
                });

                Promise.all(promises)
                    .then(() => {
                        resolve({
                            redirectLocation: redirectLocation,
                            mainComponent: (
                                <ContextComponent rxContext={rxContext}>
                                    <RoutingContext {...renderProps}></RoutingContext>
                                </ContextComponent>
                            ),
                            rxContext: rxContext});
                    })
                    .catch(reject);
            }
        });

    });
}

