import React from 'react'
import { Router } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import RuxibleComponent from './RuxibleComponent'

let DEFAULT_ONENTER = (nextState, replaceState, callback) => {
    callback(false);
};

let DEFAULT_ONLEAVE = () => {
    return false;
};

class RuxibleRouter {

    constructor(path) {
        this.path = path;
    }

    setOnEnterRoute(onEnter) {
        this.onEnter = onEnter;
    }

    setOnLeaveRoute(onLeave) {
        this.onLeave = onLeave;
    }

    createRouterCfg() {
        return this._parseRouteConfig();
    }

    _parseRouteConfig() {
        let path = this.path;

        let toIndex = (path) => {
            let route = {
                component: path.getComponent(),
                stores: path.getStores(),
                actions: path.getActions()
            };

            if (this.onEnter)
                route.onEnter = this._createOnEnter(path);

            if (this.onLeave)
                route.onLeave = this._createOnLeave(path);

            return route;
        };

        let toRoute = (path) => {
            let route = {
                path: path.getPath(),
                component: path.getComponent(),
                stores: path.getStores(),
                actions: path.getActions()
            };

            if (this.onEnter)
                route.onEnter = this._createOnEnter(path);

            if (this.onLeave)
                route.onLeave = this._createOnLeave(path);

            if (path.childs && path.childs.length > 0)
                route.childRoutes = [];

            for(var index in path.childs) {
                let child = path.getChilds()[index];

                if (child.getIndex()) {
                    route.indexRoute = toIndex(child);
                }
                else {
                    route.childRoutes.push(toRoute(child));
                }
            }

            return route;
        };

        return toRoute(path);
    }

    _createOnEnter(path) {
        let self = this;

        return (nextState, replaceState, callback) => {
            let onEnter = path.getOnEnter() || DEFAULT_ONENTER;
            onEnter(nextState, replaceState, (isReplaceState) => {
                if (!isReplaceState)
                    self.onEnter(path, callback);
            });
        };
    }

    _createOnLeave(path) {
        return (nextState, replaceState) => {
            let onLeave = path.getOnLeave() || DEFAULT_ONLEAVE;
            if (!onLeave()) {
                this.onLeave(path);
            }
        };
    }

}

module.exports = RuxibleRouter;
