import React from 'react'
import Router from 'react-router'
import RuxibleContext from './RuxibleContext'
import RuxibleComponent from './RuxibleComponent'
import StoreContainer from './StoreContainer'
import AppDispatcher from './AppDispatcher'
import RuxibleRouter from './RuxibleRouter'
import Path from './Path'
import { getStoreType, executeActionType } from './ContextType.js'
import { createHistory } from 'history'

let createElement = (Component, props) => {
    return (
        <RuxibleComponent rxStores={props.route.stores}>
            <Component {...props} />
        </RuxibleComponent>
    );
}

class Ruxible extends React.Component {

    static defaultProps = {
        onUpdate: () => {},
        rxContext: new RuxibleContext(new StoreContainer(), new AppDispatcher())
    };

    static childContextTypes = {
        rxContext: React.PropTypes.instanceOf(RuxibleContext)
    };

    constructor(props, context) {
        super(props, context);

        this.router =  new RuxibleRouter(Path.createPath(props.path));
    }

    getChildContext() {
        return {
            rxContext: this.props.rxContext,
        };
    }

    render() {
        let history = this.props.history || createHistory();
        let onUpdate = this.props.onUpdate;
        let routes = this.router.createRouterCfg();

        let routerProps = { history, routes, onUpdate, createElement };

        return (
            <Router {...routerProps} ></Router>
        );
    }

    serialize() {
        let rxContext = this.props.rxContext;

        return {
            context: rxContext.serialize()
        }
    }

    deserialize(obj) {
        let rxContext = this.props.rxContext;
        rxContext.deserialize(obj.context);
    }

};

module.exports = Ruxible;
