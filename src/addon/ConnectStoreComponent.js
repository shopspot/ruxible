import React from 'react';
import ContextType from '../ContextType.js';

module.exports = (wrapeeClass, storeClasses, getStateFromStore) => {

    var Wrapper = React.createClass({
        contextTypes: {
            getStore: ContextType.getStoreType
        },

        getInitialState: function() {
            return this.getStateFromStore();
        },

        componentWillReceiveProps: function() {
            storeClasses.forEach(storeClass => {
                this.context.getStore(storeClass).addChangeListener(this._onChange);
            }, this);
        },

        componentDidMount: function() {
            storeClasses.forEach(storeClass => {
                this.context.getStore(storeClass).addChangeListener(this._onChange);
            }, this);
        },

        componentWillUnmount: function() {
            storeClasses.forEach(storeClass => {
                let store = this.context.getStore(storeClass);
                if (store) {
                    store.removeChangeListener(this._onChange);
                }
            }, this);
        },

        render: function() {
            return React.createElement(wrapeeClass, Object.assign({}, this.props, this.state));
        },

        getStateFromStore: function() {
            if ('function' !== typeof getStateFromStore) throw Error('getStateFromStore need to be function');

            var storeInstances = {};
            storeClasses.forEach(storeClass => {
                storeInstances[storeClass.storeName] = this.context.getStore(storeClass);
            }, this);

            return getStateFromStore(storeInstances, this.props);
        },

        _onChange: function() {
            if (this.isMounted()) {
                this.setState(this.getStateFromStore());
            }
        }
    });

    return Wrapper;
};
