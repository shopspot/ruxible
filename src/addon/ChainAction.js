class ChainAction {

    constructor(context) {
        this.context = context;
        this.actions = [];
    }

    next(action, payload) {
        this.actions.push({
            action: action,
            payload: payload
        });

        return this;
    }

    execute() {
        let ctx = this.context;
        let acts = this.actions;

        if (acts.length <= 0) {
            return new Promise((resolve) => {
                resolve();
            });
        }

        let act = acts.shift();
        let promise = ctx.executeAction(act.action, act.payload);

        for (let index in acts) {
            let act = acts[index];

            promise = promise.then((result) => {
                let payload = act.payload || result;
                return ctx.executeAction(act.action, payload);
            });
        }

        return promise;
    }

};

module.exports = (actCtx) => {
    let isActionContext = (context) => {
        return context.executeAction && context.getStore && context.dispatch;
    };

    if (!isActionContext(actCtx)) {
        throw new Error('Require action context in context param');
    }

    return new ChainAction(actCtx);
};
