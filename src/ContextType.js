import React from 'react';

module.exports = {
    getStoreType: React.PropTypes.func.isRequired,
    executeActionType: React.PropTypes.func.isRequired
}
