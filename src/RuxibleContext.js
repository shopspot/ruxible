import StoreContainer from './StoreContainer'
import AppDispatcher from './AppDispatcher'

class RuxibleContext {

    constructor(storeContainer, dispatcher) {
        this._storeContainer = storeContainer || new StoreContainer();
        this._dispatcher = dispatcher || new AppDispatcher();
    }

    getContext() {
        return {
            executeAction: this.asyncExecuteAction.bind(this)
        };
    }

    getActionContext() {
        return {
            executeAction: this.asyncExecuteAction.bind(this),
            getStore: this._storeContainer.getStore.bind(this._storeContainer),
            dispatch: this._dispatcher.dispatch.bind(this._dispatcher)
        };
    }

    getComponentContext() {
        return {
            executeAction: this.syncExecuteAction.bind(this),
            getStore: this._storeContainer.getStore.bind(this._storeContainer)
        };
    }

    registerStore(storeClass) {
        if (this._storeContainer.getStore(storeClass)) {
            return;
        }

        this._storeContainer.registerStore(storeClass);

        let events = storeClass.events || {};
        let depends = storeClass.dependencies || {};
        let store = this._storeContainer.getStore(storeClass);

        for (let action in events) {
            let handlerName = events[action];
            this._dispatcher.register(action, store[handlerName].bind(store));
        }

        for (let action in depends) {
            let actDepends = depends[action] || [];
            this._dispatcher.setDependencies(action, actDepends);
        }
    }

    unregisterStore(storeClass) {
        let events = storeClass.events || {};

        this._storeContainer.unregisterStore(storeClass);

        for (let action in events) {
            this._dispatcher.unregister(action);
        }
    }

    asyncExecuteAction(action, payload, callback) {
        let promise = this._executeAction(this.getActionContext(), action, payload);
        if (callback && callback instanceof Function) {
            promise.then((result) => {
                callback(null, result);
            }, (err) => {
                callback(err, null);
            });

            return;
        }

        return promise;
    }

    syncExecuteAction(action, payload) {
        this._executeAction(this.getActionContext(), action, payload);
    }

    _executeAction(context, action, payload) {
        let promise = new Promise((resolve, reject) => {
            let pAction = action(context, payload, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });

            if (pAction instanceof Promise) {
                pAction.then(resolve, reject);
            }
        });

        return promise;
    }

    serialize() {
        return {
            stores: this._storeContainer.serialize(),
        }
    }

    deserialize(obj) {
        this._storeContainer.deserialize(obj.stores);
    }

}

module.exports = RuxibleContext;
