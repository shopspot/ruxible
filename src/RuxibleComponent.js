import React from 'react'
import RuxibleContext from './RuxibleContext'
import { getStoreType, executeActionType } from './ContextType'

class RuxibleComponent extends React.Component {
    static contextTypes = {
        rxContext: React.PropTypes.instanceOf(RuxibleContext)
    };

    static childContextTypes = {
        getStore: getStoreType,
        executeAction: executeActionType,
    };

    constructor(props) {
        super(props);
    }

    getChildContext() {
        let rxContext = this.context.rxContext;
        let compContext = rxContext.getComponentContext();

        return {
            getStore: compContext.getStore,
            executeAction: compContext.executeAction
        };
    }

    componentWillMount() {
        let rxContext = this.context.rxContext;
        let stores = this.props.rxStores || [];

        for (let index in stores) {
            rxContext.registerStore(stores[index]);
        }
    }

    componentWillUnmount() {
        let rxContext = this.context.rxContext;
        let stores = this.props.rxStores || [];

        for (let index in stores) {
            rxContext.unregisterStore(stores[index]);
        }
    }

    render() {
        return React.cloneElement(this.props.children, {});
    }

};

module.exports = RuxibleComponent;
