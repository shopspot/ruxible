'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChainAction = function () {
    function ChainAction(context) {
        _classCallCheck(this, ChainAction);

        this.context = context;
        this.actions = [];
    }

    _createClass(ChainAction, [{
        key: 'next',
        value: function next(action, payload) {
            this.actions.push({
                action: action,
                payload: payload
            });

            return this;
        }
    }, {
        key: 'execute',
        value: function execute() {
            var ctx = this.context;
            var acts = this.actions;

            if (acts.length <= 0) {
                return new Promise(function (resolve) {
                    resolve();
                });
            }

            var act = acts.shift();
            var promise = ctx.executeAction(act.action, act.payload);

            var _loop = function _loop(index) {
                var act = acts[index];

                promise = promise.then(function (result) {
                    var payload = act.payload || result;
                    return ctx.executeAction(act.action, payload);
                });
            };

            for (var index in acts) {
                _loop(index);
            }

            return promise;
        }
    }]);

    return ChainAction;
}();

;

module.exports = function (actCtx) {
    var isActionContext = function isActionContext(context) {
        return context.executeAction && context.getStore && context.dispatch;
    };

    if (!isActionContext(actCtx)) {
        throw new Error('Require action context in context param');
    }

    return new ChainAction(actCtx);
};
//# sourceMappingURL=ChainAction.js.map
