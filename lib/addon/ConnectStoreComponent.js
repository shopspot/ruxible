'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ContextType = require('../ContextType.js');

var _ContextType2 = _interopRequireDefault(_ContextType);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (wrapeeClass, storeClasses, _getStateFromStore) {

    var Wrapper = _react2.default.createClass({
        displayName: 'Wrapper',

        contextTypes: {
            getStore: _ContextType2.default.getStoreType
        },

        getInitialState: function getInitialState() {
            return this.getStateFromStore();
        },

        componentWillReceiveProps: function componentWillReceiveProps() {
            var _this = this;

            storeClasses.forEach(function (storeClass) {
                _this.context.getStore(storeClass).addChangeListener(_this._onChange);
            }, this);
        },

        componentDidMount: function componentDidMount() {
            var _this2 = this;

            storeClasses.forEach(function (storeClass) {
                _this2.context.getStore(storeClass).addChangeListener(_this2._onChange);
            }, this);
        },

        componentWillUnmount: function componentWillUnmount() {
            var _this3 = this;

            storeClasses.forEach(function (storeClass) {
                var store = _this3.context.getStore(storeClass);
                if (store) {
                    store.removeChangeListener(_this3._onChange);
                }
            }, this);
        },

        render: function render() {
            return _react2.default.createElement(wrapeeClass, Object.assign({}, this.props, this.state));
        },

        getStateFromStore: function getStateFromStore() {
            var _this4 = this;

            if ('function' !== typeof _getStateFromStore) throw Error('getStateFromStore need to be function');

            var storeInstances = {};
            storeClasses.forEach(function (storeClass) {
                storeInstances[storeClass.storeName] = _this4.context.getStore(storeClass);
            }, this);

            return _getStateFromStore(storeInstances, this.props);
        },

        _onChange: function _onChange() {
            if (this.isMounted()) {
                this.setState(this.getStateFromStore());
            }
        }
    });

    return Wrapper;
};
//# sourceMappingURL=ConnectStoreComponent.js.map
