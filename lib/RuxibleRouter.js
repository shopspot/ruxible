'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _createBrowserHistory = require('history/lib/createBrowserHistory');

var _createBrowserHistory2 = _interopRequireDefault(_createBrowserHistory);

var _RuxibleComponent = require('./RuxibleComponent');

var _RuxibleComponent2 = _interopRequireDefault(_RuxibleComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DEFAULT_ONENTER = function DEFAULT_ONENTER(nextState, replaceState, callback) {
    callback(false);
};

var DEFAULT_ONLEAVE = function DEFAULT_ONLEAVE() {
    return false;
};

var RuxibleRouter = function () {
    function RuxibleRouter(path) {
        _classCallCheck(this, RuxibleRouter);

        this.path = path;
    }

    _createClass(RuxibleRouter, [{
        key: 'setOnEnterRoute',
        value: function setOnEnterRoute(onEnter) {
            this.onEnter = onEnter;
        }
    }, {
        key: 'setOnLeaveRoute',
        value: function setOnLeaveRoute(onLeave) {
            this.onLeave = onLeave;
        }
    }, {
        key: 'createRouterCfg',
        value: function createRouterCfg() {
            return this._parseRouteConfig();
        }
    }, {
        key: '_parseRouteConfig',
        value: function _parseRouteConfig() {
            var _this = this;

            var path = this.path;

            var toIndex = function toIndex(path) {
                var route = {
                    component: path.getComponent(),
                    stores: path.getStores(),
                    actions: path.getActions()
                };

                if (_this.onEnter) route.onEnter = _this._createOnEnter(path);

                if (_this.onLeave) route.onLeave = _this._createOnLeave(path);

                return route;
            };

            var toRoute = function toRoute(path) {
                var route = {
                    path: path.getPath(),
                    component: path.getComponent(),
                    stores: path.getStores(),
                    actions: path.getActions()
                };

                if (_this.onEnter) route.onEnter = _this._createOnEnter(path);

                if (_this.onLeave) route.onLeave = _this._createOnLeave(path);

                if (path.childs && path.childs.length > 0) route.childRoutes = [];

                for (var index in path.childs) {
                    var child = path.getChilds()[index];

                    if (child.getIndex()) {
                        route.indexRoute = toIndex(child);
                    } else {
                        route.childRoutes.push(toRoute(child));
                    }
                }

                return route;
            };

            return toRoute(path);
        }
    }, {
        key: '_createOnEnter',
        value: function _createOnEnter(path) {
            var self = this;

            return function (nextState, replaceState, callback) {
                var onEnter = path.getOnEnter() || DEFAULT_ONENTER;
                onEnter(nextState, replaceState, function (isReplaceState) {
                    if (!isReplaceState) self.onEnter(path, callback);
                });
            };
        }
    }, {
        key: '_createOnLeave',
        value: function _createOnLeave(path) {
            var _this2 = this;

            return function (nextState, replaceState) {
                var onLeave = path.getOnLeave() || DEFAULT_ONLEAVE;
                if (!onLeave()) {
                    _this2.onLeave(path);
                }
            };
        }
    }]);

    return RuxibleRouter;
}();

module.exports = RuxibleRouter;
//# sourceMappingURL=RuxibleRouter.js.map
