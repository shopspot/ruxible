'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RuxibleContext = require('./RuxibleContext');

var _RuxibleContext2 = _interopRequireDefault(_RuxibleContext);

var _ContextType = require('./ContextType');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RuxibleComponent = function (_React$Component) {
    _inherits(RuxibleComponent, _React$Component);

    function RuxibleComponent(props) {
        _classCallCheck(this, RuxibleComponent);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(RuxibleComponent).call(this, props));
    }

    _createClass(RuxibleComponent, [{
        key: 'getChildContext',
        value: function getChildContext() {
            var rxContext = this.context.rxContext;
            var compContext = rxContext.getComponentContext();

            return {
                getStore: compContext.getStore,
                executeAction: compContext.executeAction
            };
        }
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            var rxContext = this.context.rxContext;
            var stores = this.props.rxStores || [];

            for (var index in stores) {
                rxContext.registerStore(stores[index]);
            }
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            var rxContext = this.context.rxContext;
            var stores = this.props.rxStores || [];

            for (var index in stores) {
                rxContext.unregisterStore(stores[index]);
            }
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.cloneElement(this.props.children, {});
        }
    }]);

    return RuxibleComponent;
}(_react2.default.Component);

RuxibleComponent.contextTypes = {
    rxContext: _react2.default.PropTypes.instanceOf(_RuxibleContext2.default)
};
RuxibleComponent.childContextTypes = {
    getStore: _ContextType.getStoreType,
    executeAction: _ContextType.executeActionType
};
;

module.exports = RuxibleComponent;
//# sourceMappingURL=RuxibleComponent.js.map
