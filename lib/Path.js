"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Path = function () {
    function Path(path, component) {
        _classCallCheck(this, Path);

        this.path = path || "";
        this.component = component;
        this.actions = [];
        this.stores = [];
        this.childs = [];
        this.index = false;
    }

    _createClass(Path, [{
        key: "getPath",
        value: function getPath() {
            return this.path;
        }
    }, {
        key: "getComponent",
        value: function getComponent() {
            return this.component;
        }
    }, {
        key: "setAction",
        value: function setAction(act, payload) {
            this.actions.push({
                action: act,
                payload: payload
            });
        }
    }, {
        key: "getActions",
        value: function getActions() {
            return this.actions;
        }
    }, {
        key: "addStore",
        value: function addStore(store) {
            this.stores.push(store);
        }
    }, {
        key: "getStores",
        value: function getStores() {
            return this.stores;
        }
    }, {
        key: "addChild",
        value: function addChild(child) {
            this.childs.push(child);
        }
    }, {
        key: "getChilds",
        value: function getChilds() {
            return this.childs;
        }
    }, {
        key: "setIndex",
        value: function setIndex() {
            this.index = true;
        }
    }, {
        key: "getIndex",
        value: function getIndex() {
            return this.index;
        }
    }, {
        key: "setOnEnter",
        value: function setOnEnter(onEnter) {
            this.onEnter = onEnter;
        }
    }, {
        key: "getOnEnter",
        value: function getOnEnter() {
            return this.onEnter;
        }
    }, {
        key: "setOnLeave",
        value: function setOnLeave(onLeave) {
            this.onLeave = onLeave;
        }
    }, {
        key: "getOnLeave",
        value: function getOnLeave() {
            return this.onLeave;
        }
    }], [{
        key: "createPath",
        value: function createPath(pathConfig) {
            var makePath = function makePath(pathCfg) {
                var path = new Path(pathCfg.path, pathCfg.component);
                if (pathCfg.index && pathCfg.index === true) {
                    path.setIndex();
                }

                var stores = pathCfg.stores || [];
                for (var index in stores) {
                    var store = stores[index];
                    path.addStore(store);
                }

                var childs = pathCfg.childs || [];
                for (var index in childs) {
                    var child = childs[index];
                    path.addChild(makePath(child));
                }

                var actions = pathCfg.actions || [];
                for (var index in actions) {
                    var action = actions[index];
                    path.setAction(action.action, action.payload);
                }

                if (pathCfg.onEnter) {
                    path.setOnEnter(pathCfg.onEnter);
                }

                if (pathCfg.onLeave) {
                    path.setOnLeave(pathCfg.onLeave);
                }

                return path;
            };

            return makePath(pathConfig);
        }
    }]);

    return Path;
}();

;

module.exports = Path;
//# sourceMappingURL=Path.js.map
