'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
    getStoreType: _react2.default.PropTypes.func.isRequired,
    executeActionType: _react2.default.PropTypes.func.isRequired
};
//# sourceMappingURL=ContextType.js.map
