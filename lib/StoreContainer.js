'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _BaseStore = require('./BaseStore.js');

var _BaseStore2 = _interopRequireDefault(_BaseStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StoreContainer = function () {
    function StoreContainer() {
        _classCallCheck(this, StoreContainer);

        this._stores = {};
        this._rawStores = {};
    }

    _createClass(StoreContainer, [{
        key: 'registerStore',
        value: function registerStore(storeClass) {
            var rawStores = this._rawStores;
            var stores = this._stores;
            var storeName = storeClass.storeName;
            var store = new storeClass();

            if (rawStores[storeName]) {
                store.deserialize(rawStores[storeName]);
            }

            stores[storeName] = store;
        }
    }, {
        key: 'unregisterStore',
        value: function unregisterStore(storeClass) {
            var rawStores = this._rawStores;
            var stores = this._stores;
            var storeName = storeClass.storeName;

            delete stores[storeName];
            delete rawStores[storeName];
        }
    }, {
        key: 'getStore',
        value: function getStore(storeClass) {
            var storeName = storeClass.storeName || storeClass;
            var store = this._stores[storeName];
            var rawStore = this._rawStores[storeName];

            if (!store && rawStore) {
                this.registerStore(storeClass);
                store = this._stores[storeName];
            }

            return store;
        }
    }, {
        key: 'serialize',
        value: function serialize() {
            var context = {
                stores: {}
            };

            var stores = this._stores;
            for (var key in stores) {
                if (stores.hasOwnProperty(key)) {
                    var store = this.getStore(key);
                    context.stores[key] = store.serialize();
                }
            }

            return context;
        }
    }, {
        key: 'deserialize',
        value: function deserialize(context) {
            var rawStores = this._rawStores;

            var stores = context.stores;
            for (var key in stores) {
                if (stores.hasOwnProperty(key)) {
                    rawStores[key] = stores[key];
                }
            }
        }
    }]);

    return StoreContainer;
}();

module.exports = StoreContainer;
//# sourceMappingURL=StoreContainer.js.map
