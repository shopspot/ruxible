'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _flux = require('flux');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DEFAULT_CHANNEL = 'default';

var ChannelDispatcher = function () {
    function ChannelDispatcher() {
        _classCallCheck(this, ChannelDispatcher);

        this._dispatcher = new _flux.Dispatcher();
        this._channels = {};
    }

    _createClass(ChannelDispatcher, [{
        key: 'register',
        value: function register(channel, cb, dependencies) {
            dependencies = dependencies || [];
            if (!cb) {
                cb = channel;
                channel = DEFAULT_CHANNEL;
            }

            this.validate(channel, dependencies);

            this._channels[channel] = {
                handler: cb,
                dependencies: dependencies
            };
        }
    }, {
        key: 'unregister',
        value: function unregister(channel) {
            delete this._channels[channel];
        }
    }, {
        key: 'setDependencies',
        value: function setDependencies(channel, dependencies) {
            if (!this.isExistsChannel(channel)) {
                throw new Error('Channel "' + channel + '" is not exists');
            }

            this._channels[channel].dependencies = dependencies;
        }
    }, {
        key: 'validate',
        value: function validate(channel, dependencies) {
            if (this.isExistsChannel(channel)) {
                throw new Error('Channel already exists');
            }

            for (var index in dependencies) {
                if (!this.isExistsChannel(dependencies[index])) throw new Error('Dependencies "' + dependencies[index] + '" doesn\' exists in channel');
            }
        }
    }, {
        key: 'dispatch',
        value: function dispatch(channelName, payload) {
            var _this = this;

            if (!payload) {
                payload = channelName;
                channelName = DEFAULT_CHANNEL;
            }

            if (!this.isExistsChannel(channelName)) {
                return;
            }

            var channel = this._channels[channelName];
            var depIds = [];
            var depends = channel.dependencies;

            for (var index in depends) {
                var depCh = this._channels[depends[index]];
                var _id = this._dispatcher.register(depCh.handler);
                depIds.push(_id);
            }

            var id = 0;
            if (depIds.length > 0) {
                id = this._dispatcher.register(function (payload) {
                    _this.waitFor(depIds);
                    channel.handler(payload);
                });
            } else {
                id = this._dispatcher.register(channel.handler);
            }

            this._dispatcher.dispatch(payload);

            this._dispatcher.unregister(id);
            for (var index in depIds) {
                this._dispatcher.unregister(depIds[index]);
            }
        }
    }, {
        key: 'waitFor',
        value: function waitFor(ids) {
            this._dispatcher.waitFor(ids);
        }
    }, {
        key: 'isMultipleArgs',
        value: function isMultipleArgs(args) {
            return args.length > 1;
        }
    }, {
        key: 'isExistsChannel',
        value: function isExistsChannel(channel) {
            return this._channels[channel] ? true : false;
        }
    }]);

    return ChannelDispatcher;
}();

module.exports = ChannelDispatcher;
//# sourceMappingURL=AppDispatcher.js.map
