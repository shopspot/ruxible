'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _StoreContainer = require('./StoreContainer');

var _StoreContainer2 = _interopRequireDefault(_StoreContainer);

var _AppDispatcher = require('./AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RuxibleContext = function () {
    function RuxibleContext(storeContainer, dispatcher) {
        _classCallCheck(this, RuxibleContext);

        this._storeContainer = storeContainer || new _StoreContainer2.default();
        this._dispatcher = dispatcher || new _AppDispatcher2.default();
    }

    _createClass(RuxibleContext, [{
        key: 'getContext',
        value: function getContext() {
            return {
                executeAction: this.asyncExecuteAction.bind(this)
            };
        }
    }, {
        key: 'getActionContext',
        value: function getActionContext() {
            return {
                executeAction: this.asyncExecuteAction.bind(this),
                getStore: this._storeContainer.getStore.bind(this._storeContainer),
                dispatch: this._dispatcher.dispatch.bind(this._dispatcher)
            };
        }
    }, {
        key: 'getComponentContext',
        value: function getComponentContext() {
            return {
                executeAction: this.syncExecuteAction.bind(this),
                getStore: this._storeContainer.getStore.bind(this._storeContainer)
            };
        }
    }, {
        key: 'registerStore',
        value: function registerStore(storeClass) {
            if (this._storeContainer.getStore(storeClass)) {
                return;
            }

            this._storeContainer.registerStore(storeClass);

            var events = storeClass.events || {};
            var depends = storeClass.dependencies || {};
            var store = this._storeContainer.getStore(storeClass);

            for (var action in events) {
                var handlerName = events[action];
                this._dispatcher.register(action, store[handlerName].bind(store));
            }

            for (var action in depends) {
                var actDepends = depends[action] || [];
                this._dispatcher.setDependencies(action, actDepends);
            }
        }
    }, {
        key: 'unregisterStore',
        value: function unregisterStore(storeClass) {
            var events = storeClass.events || {};

            this._storeContainer.unregisterStore(storeClass);

            for (var action in events) {
                this._dispatcher.unregister(action);
            }
        }
    }, {
        key: 'asyncExecuteAction',
        value: function asyncExecuteAction(action, payload, callback) {
            var promise = this._executeAction(this.getActionContext(), action, payload);
            if (callback && callback instanceof Function) {
                promise.then(function (result) {
                    callback(null, result);
                }, function (err) {
                    callback(err, null);
                });

                return;
            }

            return promise;
        }
    }, {
        key: 'syncExecuteAction',
        value: function syncExecuteAction(action, payload) {
            this._executeAction(this.getActionContext(), action, payload);
        }
    }, {
        key: '_executeAction',
        value: function _executeAction(context, action, payload) {
            var promise = new Promise(function (resolve, reject) {
                var pAction = action(context, payload, function (err, result) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });

                if (pAction instanceof Promise) {
                    pAction.then(resolve, reject);
                }
            });

            return promise;
        }
    }, {
        key: 'serialize',
        value: function serialize() {
            return {
                stores: this._storeContainer.serialize()
            };
        }
    }, {
        key: 'deserialize',
        value: function deserialize(obj) {
            this._storeContainer.deserialize(obj.stores);
        }
    }]);

    return RuxibleContext;
}();

module.exports = RuxibleContext;
//# sourceMappingURL=RuxibleContext.js.map
