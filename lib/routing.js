'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _RuxibleContext = require('./RuxibleContext');

var _RuxibleContext2 = _interopRequireDefault(_RuxibleContext);

var _RuxibleComponent = require('./RuxibleComponent');

var _RuxibleComponent2 = _interopRequireDefault(_RuxibleComponent);

var _StoreContainer = require('./StoreContainer');

var _StoreContainer2 = _interopRequireDefault(_StoreContainer);

var _AppDispatcher = require('./AppDispatcher');

var _AppDispatcher2 = _interopRequireDefault(_AppDispatcher);

var _RuxibleRouter = require('./RuxibleRouter');

var _RuxibleRouter2 = _interopRequireDefault(_RuxibleRouter);

var _Path = require('./Path');

var _Path2 = _interopRequireDefault(_Path);

var _ChainAction = require('./addon/ChainAction');

var _ChainAction2 = _interopRequireDefault(_ChainAction);

var _ContextType = require('./ContextType.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var createElement = function createElement(Component, props) {
    return _react2.default.createElement(
        _RuxibleComponent2.default,
        { rxStores: props.route.stores },
        _react2.default.createElement(Component, props)
    );
};

var ContextComponent = function (_React$Component) {
    _inherits(ContextComponent, _React$Component);

    function ContextComponent(props) {
        _classCallCheck(this, ContextComponent);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(ContextComponent).call(this, props));
    }

    _createClass(ContextComponent, [{
        key: 'getChildContext',
        value: function getChildContext() {
            return {
                rxContext: this.props.rxContext
            };
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.cloneElement(this.props.children, {});
        }
    }]);

    return ContextComponent;
}(_react2.default.Component);

ContextComponent.childContextTypes = {
    rxContext: _react2.default.PropTypes.instanceOf(_RuxibleContext2.default)
};
;

module.exports = function (path, url) {
    var rxContext = new _RuxibleContext2.default(new _StoreContainer2.default(), new _AppDispatcher2.default());
    var router = new _RuxibleRouter2.default(_Path2.default.createPath(path));

    return new Promise(function (resolve, reject) {
        (0, _reactRouter.match)({ routes: router.createRouterCfg(), location: url }, function (error, redirectLocation, renderProps) {
            if (error) reject(error);else {
                (function () {
                    renderProps.createElement = createElement;

                    var routes = renderProps.routes;
                    var promises = [];

                    routes.forEach(function (route) {
                        var actions = route.actions || [];
                        var stores = route.stores || [];

                        var chain = (0, _ChainAction2.default)(rxContext.getActionContext());

                        stores.forEach(function (store) {
                            rxContext.registerStore(store);
                        });

                        actions.forEach(function (action) {
                            var payload = action.payload || {};
                            payload.params = renderProps.params;
                            payload.location = renderProps.location;

                            chain.next(action.action, payload);
                        });

                        promises.push(chain.execute());
                    });

                    Promise.all(promises).then(function () {
                        resolve({
                            redirectLocation: redirectLocation,
                            mainComponent: _react2.default.createElement(
                                ContextComponent,
                                { rxContext: rxContext },
                                _react2.default.createElement(_reactRouter.RoutingContext, renderProps)
                            ),
                            rxContext: rxContext });
                    }).catch(reject);
                })();
            }
        });
    });
};
//# sourceMappingURL=routing.js.map
